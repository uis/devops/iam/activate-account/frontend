/** @type {import('next').NextConfig} */
const nextConfig = {
  // This is set at build time, there currently doesn't appear to be a way to set this at runtime
  // to dynamically set it based on the environment
  basePath: process.env.HOSTED_PATH || "",
  assetPrefix: `${process.env.HOSTED_PATH || ""}/`,
  trailingSlash: true,

  output: "export",
  images: {
    unoptimized: true,
  },
};

export default nextConfig;
