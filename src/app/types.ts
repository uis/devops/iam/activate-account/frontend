import { identifiers, steps } from "./consts";

export type Identifiers = (typeof identifiers)[keyof typeof identifiers];

export type Steps = (typeof steps)[keyof typeof steps];
