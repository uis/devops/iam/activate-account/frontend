import { AxiosError } from "axios";
import type { Logger } from "pino";
import { steps } from "../consts";
import useStore from "../store";

export const handleErrorResponse = (error: unknown, logger: Logger | null) => {
  if (error instanceof AxiosError && error.response) {
    if (error.response.status === 404) {
      if (logger) {
        logger.error("Request failed with 404 status", {
          error,
        });
      }

      useStore.getState().setStep(steps.error);
      return true;
    } else if (error.response.status >= 500) {
      if (logger) {
        logger.error("Request failed with 500 status", {
          error,
        });
      }

      useStore.getState().setStep(steps.error);
      return true;
    }
  }

  return false;
};
