import type { AccountManagementApi } from "activate-account";

export type Session = {
  accessToken: string;
};

export type AuthData = {
  dateOfBirth: string;
  code: string;
  crsid?: string;
  lastName?: string;
};

export type AuthConfig = {
  authenticate: (data: AuthData) => Promise<void>;
  client: AccountManagementApi;
  revoke: () => Promise<void>;
};
