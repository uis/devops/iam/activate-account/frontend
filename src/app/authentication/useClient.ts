"use client";

import { Configuration } from "activate-account";
import type { BaseAPI } from "activate-account/base";
import axios from "axios";
import { useMemo, useRef } from "react";
import { useConfig } from "../config/ConfigProvider";
import { steps } from "../consts";
import { useLogger } from "../logger/LoggerProvider";
import useStore from "../store";
import { handleErrorResponse } from "./errors";
import { Session } from "./types";

const useClient = <T>(
  session: Session | null,
  api: new (...args: ConstructorParameters<typeof BaseAPI>) => T,
  {
    skipSessionExpiredCheck = false,
  }: {
    skipSessionExpiredCheck?: boolean;
  } = {},
): T => {
  const configuration = useMemo(
    () => (session ? new Configuration({ accessToken: session.accessToken }) : undefined),
    [session],
  );

  const logger = useLogger();
  const loggerRef = useRef(logger);
  loggerRef.current = logger;

  const client = useMemo(() => {
    const client = axios.create();

    client.interceptors.response.use(
      (response) => response,
      (error) => {
        if (
          !skipSessionExpiredCheck &&
          error.config.headers.Authorization &&
          error?.response?.status === 401 &&
          error?.response?.data?.error === "invalid_token"
        ) {
          // We did pass an Authorization header, but it was invalid. So the token must have been expired.
          useStore.getState().setStep(steps.sessionExpired);
        } else if (!handleErrorResponse(error, loggerRef.current)) {
          // Return the error so that the caller can handle it
          return Promise.reject(error);
        }
      },
    );

    return client;
  }, [skipSessionExpiredCheck]);

  const config = useConfig();

  return useMemo(
    () => new api(configuration, config.API_BASE_PATH, client),
    [config, configuration, client, api],
  );
};

export default useClient;
