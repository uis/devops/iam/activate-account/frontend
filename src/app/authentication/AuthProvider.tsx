import { AccountManagementApi, AuthenticationApi } from "activate-account";
import {
  createContext,
  PropsWithChildren,
  useCallback,
  useContext,
  useMemo,
  useState,
} from "react";
import { useReCaptcha } from "../captcha/ReCaptchaProvider";
import { AuthConfig, AuthData, Session } from "./types";
import useClient from "./useClient";

const AuthContext = createContext<AuthConfig | null>(null);

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth must be used within a AuthProvider");
  }
  return context;
};

const GRANT_TYPE = "urn:devops.uis.cam.ac.uk:params:oauth:grant-type:new-user-credentials";

const AuthProvider = ({ children }: PropsWithChildren) => {
  const [session, setSessionState] = useState<Session | null>(null);
  const actionTokenRef = useReCaptcha();

  const client = useClient(session, AccountManagementApi);
  const authClient = useClient(session, AuthenticationApi, { skipSessionExpiredCheck: true });

  const setSession = useCallback(
    async (data: Session | null) => {
      if (session) {
        await authClient.revokeTokenSession();
      }

      setSessionState(data);
    },
    [session, setSessionState, authClient],
  );

  const revoke = useCallback(() => setSession(null), [setSession]);

  const authenticate = useCallback(
    async (data: AuthData) => {
      await setSession(null);

      return authClient
        .getToken(GRANT_TYPE, data.dateOfBirth, data.code, data.crsid, data.lastName, {
          headers: {
            "X-Recaptcha-Token": actionTokenRef.current,
          },
        })
        .then(({ data }) =>
          setSession({
            accessToken: data.access_token,
          }),
        );
    },
    [setSession, actionTokenRef, authClient],
  );

  const value = useMemo(
    () => ({
      authenticate,
      client,
      revoke,
    }),
    [authenticate, client, revoke],
  );

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export default AuthProvider;
