import Script from "next/script";
import {
  createContext,
  PropsWithChildren,
  useCallback,
  useContext,
  useRef,
  RefObject,
} from "react";

import { useConfig } from "../config/ConfigProvider";

const ReCaptchaContext = createContext<RefObject<string> | null>(null);

export const useReCaptcha = () => {
  const context = useContext(ReCaptchaContext);
  if (!context) {
    throw new Error("useReCaptcha must be used within a ReCaptchaProvider");
  }
  return context;
};

const ReCaptchaProvider = ({ children }: PropsWithChildren) => {
  const config = useConfig();
  const actionToken = useRef("");

  const getActionToken = useCallback(() => {
    const onSuccess = (value: string) => {
      actionToken.current = value;
    };
    const onError = (reason: string) => {
      throw new Error(reason);
    };
    window.grecaptcha.enterprise.ready(() => {
      window.grecaptcha.enterprise
        .execute(config.RECAPTCHA_KEY, { action: "AUTHENTICATE" })
        .then(onSuccess, onError);
    });
  }, [config.RECAPTCHA_KEY]);

  return (
    <>
      {config?.RECAPTCHA_KEY && (
        <Script
          src={`https://www.google.com/recaptcha/enterprise.js?render=${config.RECAPTCHA_KEY}`}
          onReady={getActionToken}
        />
      )}
      <ReCaptchaContext.Provider value={actionToken}>{children}</ReCaptchaContext.Provider>
    </>
  );
};

export default ReCaptchaProvider;
