"use client";

// MUI components
import {
  Box,
  Button,
  Container,
  Link,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";

// Constants
import {
  guidanceOnAccountAndPasswords,
  registrationProcessForStudents,
  userAdminEmail,
} from "../consts";

// Custom hooks
import useStore from "../store";

const UniversityAccountRegistration = () => (
  <Container>
    <Typography variant="h1">Get your University IT account sign in details</Typography>
    <Typography variant="bodyLargeText" component="p" gutterBottom>
      You need to set up your University IT account sign in details.
    </Typography>
    <Typography variant="bodyLargeText">
      You will be able to use your sign in details to access:
    </Typography>
    <List className="MuiList-unordered">
      {[
        "University email address",
        "wifi internet",
        "software",
        "shared computers and printers",
      ].map((item) => (
        <ListItem key={item}>
          <ListItemText primary={item} />
        </ListItem>
      ))}
    </List>
    <Typography variant="h4">Before you start</Typography>
    <Typography variant="bodyLargeText">You&apos;ll need:</Typography>
    <List className="MuiList-unordered">
      {[
        "your CRSid, if you have it",
        "a temporary registration code, if you're a member of staff",
        "an admissions number, if you're a student",
      ].map((item) => (
        <ListItem key={item}>
          <ListItemText primary={item} />
        </ListItem>
      ))}
    </List>
    <Typography variant="bodyLargeText" component="p" gutterBottom>
      Guidance on{" "}
      <Link href={guidanceOnAccountAndPasswords} target="_blank">
        your CRSid (opens in a new tab)
      </Link>{" "}
      is available.
    </Typography>
    <Typography variant="bodyLargeText" component="p" gutterBottom>
      Information on admissions numbers is included in the{" "}
      <Link href={registrationProcessForStudents} target="_blank">
        registration process for students (opens in a new tab)
      </Link>
      .
    </Typography>
    <Typography variant="h4">If you need help</Typography>
    <Typography variant="bodyLargeText" component="p" gutterBottom>
      Contact <Link href={`mailto:${userAdminEmail}`}>{userAdminEmail}</Link> if you have any
      problems registering your IT account.
    </Typography>
    <Box>
      <Button variant="contained" onClick={() => useStore.getState().setStep("identity")}>
        Get your University IT account sign in details
      </Button>
    </Box>
  </Container>
);

export default UniversityAccountRegistration;
