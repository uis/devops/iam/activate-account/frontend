// MUI components
import { Container, Typography, Link, List, ListItem, ListItemText } from "@mui/material";

// Custom components
import NavigationButtons from "../components/NavigationButtons";

// Constants
import { userAdminEmail, guidanceOnStudentRegistrationPortal } from "../consts";

const UniversityAccountNotFound = () => (
  <Container>
    <Typography variant="h3">University account not found</Typography>
    <Typography variant="bodyLargeText" component="p" gutterBottom>
      We could not find a University account matching the details you entered.
    </Typography>
    <Typography variant="bodyLargeText" component="p" gutterBottom>
      We cannot give you access to your University IT account unless we can match the correct
      details.
    </Typography>
    <Typography variant="h4">Why could my details not be found?</Typography>
    <Typography variant="bodyLargeText" component="p" gutterBottom={false}>
      This could be because
    </Typography>

    <List className="MuiList-unordered">
      {[
        "you entered one or more of your account details incorrectly. For example, CRSid, last name, date of birth or admissions number.",
        "your University IT account is not yet available.",
      ].map((item) => (
        <ListItem key={item}>
          <ListItemText primary={item} />
        </ListItem>
      ))}
    </List>
    <Typography variant="h4">Next steps</Typography>
    <Typography variant="bodyLargeText" component="p" gutterBottom={false}>
      To fix this problem:
    </Typography>
    <List className="MuiList-unordered">
      {[
        "re-enter your account details, making sure to avoid any mistakes.",
        "if you are a member of staff, ask your line manager, local HR or IT team if your University IT account is available.",
      ].map((item) => (
        <ListItem key={item}>
          <ListItemText primary={item} />
        </ListItem>
      ))}
      <ListItem>
        <ListItemText
          primary={
            <>
              if you are a student, sign into your{" "}
              <Link href={guidanceOnStudentRegistrationPortal} target="_blank">
                Student Registration Portal
              </Link>{" "}
              to check whether your University IT account is available.
            </>
          }
        />
      </ListItem>
    </List>
    <Typography variant="h4">Need help using this service?</Typography>
    <Typography variant="bodyLargeText" component="p" gutterBottom={false}>
      Contact <Link href={`mailto:${userAdminEmail}`}>{userAdminEmail}</Link> if you are still
      having problems.
    </Typography>
    <NavigationButtons
      previousStep="userData"
      previousLabel="Previous"
      nextStep="userData"
      nextLabel="Re-enter your account details"
    />
  </Container>
);

export default UniversityAccountNotFound;
