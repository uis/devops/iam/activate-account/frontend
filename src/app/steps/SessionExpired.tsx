// MUI components
import { Container, Typography, Button } from "@mui/material";

// Custom hooks
import useStore from "../store";

const SessionExpired = () => {
  return (
    <Container>
      <Typography variant="h1">Your session has ended</Typography>
      <Typography variant="body1" paragraph>
        This is because you did not do anything for 20 minutes. We do this to protect your
        information.
      </Typography>
      <Typography variant="body1" paragraph>
        We have deleted your answers.
      </Typography>
      <Typography variant="body1" paragraph gutterBottom>
        You will need to start again.
      </Typography>
      <Button variant="contained" onClick={() => useStore.getState().setStep("home")}>
        Start again
      </Button>
    </Container>
  );
};

export default SessionExpired;
