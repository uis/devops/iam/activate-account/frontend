"use client";

import { Account } from "activate-account";
import { FormEvent, useEffect, useState } from "react";

// MUI components
import CheckCircleOutlineOutlinedIcon from "@mui/icons-material/CheckCircleOutlineOutlined";
import ErrorIcon from "@mui/icons-material/Error";
import OpenInNewOutlinedIcon from "@mui/icons-material/OpenInNewOutlined";
import {
  Backdrop,
  Box,
  CircularProgress,
  Container,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";

// HTTP utilities
import { AxiosError } from "axios";

// Custom components
import NavigationButtons from "../components/NavigationButtons";

// Custom hooks
import { useAuth } from "../authentication/AuthProvider";
import { useConfig } from "../config/ConfigProvider";
import { useLogger } from "../logger/LoggerProvider";
import useStore from "../store";

// Constants
import { steps } from "../consts";

const UserAccountOverview = () => {
  const config = useConfig();
  const { client } = useAuth();
  const [accountDetails, setAccountDetails] = useState<Account | null>(null);
  const [token, setToken] = useState<string>("");
  const logger = useLogger();
  const setStep = useStore((state) => state.setStep);

  useEffect(() => {
    async function retrieveDetails() {
      const data = (await client.getAccount()).data;
      setAccountDetails(data);
    }
    async function getPasswordToken() {
      try {
        const data = (await client.resetToken()).data;
        setToken(data.token);
      } catch (error) {
        logger?.error("Token retrieval failed", { error });
        if (error instanceof AxiosError && error.response?.status === 400) {
          // Unable to set a token for the user typically means the account is not created yet.
          // Redirect to AccountNotFound step but may need a better message.
          setStep(steps.universityAccountNotFound);
        } else {
          setStep(steps.error);
        }
        return;
      }
    }

    retrieveDetails();
    getPasswordToken();
  }, [client, logger, setStep]);

  const handleRedirect = (event: FormEvent) => {
    event.preventDefault();
    if (accountDetails?.crsid) {
      const redirectUrl = `${config.TOKEN_REDIRECT_URL}?crsid=${accountDetails.crsid}&token=${token}`;
      window.location.href = redirectUrl;
    }
  };

  return (
    <Container component="form" autoComplete="off" onSubmit={handleRedirect}>
      {!accountDetails || !token ? (
        <Backdrop open={true}>
          <CircularProgress />
        </Backdrop>
      ) : (
        <>
          <Typography variant="h1" gutterBottom>
            <CheckCircleOutlineOutlinedIcon fontSize="large" color="primary" />
            Account verified
          </Typography>
          <Typography variant="body1" gutterBottom={false}>
            University account details for CRSid <strong>{accountDetails.crsid}</strong> have been
            verified.
          </Typography>
          <Typography variant="h4" gutterBottom={false}>
            What happens next
          </Typography>
          <Typography variant="body1" gutterBottom={false}>
            You need to:
          </Typography>
          <List className="MuiList-ordered">
            {[
              "go to the account password website and create a new password",
              "set up password recovery, in case you lose or forget your password",
            ].map((item) => (
              <ListItem key={item}>
                <ListItemText primary={item} />
              </ListItem>
            ))}
          </List>
          <Box display="flex" alignItems="center">
            <ErrorIcon fontSize="small" />
            <Typography variant="body1" gutterBottom={false}>
              &nbsp;If you do not set up your new password within 24 hours you&apos;ll need to
              verify your account again.
            </Typography>
          </Box>

          {/* Navigation Buttons */}
          <NavigationButtons
            previousStep="home"
            previousLabel="Previous"
            nextLabel={
              <>
                Create a new password&nbsp;
                <OpenInNewOutlinedIcon fontSize="small" />
              </>
            }
          />
        </>
      )}
    </Container>
  );
};

export default UserAccountOverview;
