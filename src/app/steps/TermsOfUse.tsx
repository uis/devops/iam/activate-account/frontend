"use client";

// MUI components
import { Container, Link, List, ListItem, ListItemText, Typography } from "@mui/material";

// React Hook Form utilities
import { zodResolver } from "@hookform/resolvers/zod";
import { SubmitHandler, useForm } from "react-hook-form";
import * as z from "zod";

// Constants
import { termsAndConditions } from "../consts";

// Custom components
import FormCheckbox from "../components/forms/FormCheckbox";
import NavigationButtons from "../components/NavigationButtons";

// Custom hooks
import { useAuth } from "../authentication/AuthProvider";
import { useLogger } from "../logger/LoggerProvider";
import useStore from "../store";

const schema = z.object({
  agreedToTerms: z.array(z.string()).min(1, "⚠ You must agree to the terms of use"),
});

type FormData = z.infer<typeof schema>;

const TermsOfUse = () => {
  const { client } = useAuth();
  const setStep = useStore((state) => state.setStep);
  const logger = useLogger();

  const { handleSubmit, control } = useForm<FormData>({
    resolver: zodResolver(schema),
    mode: "onBlur",
    defaultValues: {
      agreedToTerms: [],
    },
  });

  const onSubmit: SubmitHandler<FormData> = async () => {
    try {
      const result = await client.patchAccount({
        termsAccepted: true,
      });
      if (result.status === 200) {
        setStep("accountOverview");
      } else {
        logger?.error("An error occurred.");
      }
    } catch (error) {
      logger?.error("An error occurred.", { error });
    }
  };

  return (
    <Container component="form" onSubmit={handleSubmit(onSubmit)} autoComplete="off">
      <Typography variant="h3">Review and agree to the Acceptable Use Policy</Typography>
      <Typography variant="bodyLargeText" component="p" gutterBottom>
        You must read and agree to follow the Acceptable Use Policy to get a University IT account.
      </Typography>
      <Typography variant="bodyLargeText" component="p" gutterBottom>
        The Acceptable Use Policy is in place to prevent cybercrime and to keep data safe.
      </Typography>
      <Typography variant="bodyLargeText" component="p" gutterBottom={false}>
        It covers:
      </Typography>
      <List className="MuiList-unordered">
        {[
          "signing in securely and keeping sign-in details safe",
          "protecting against malware",
          "keeping data safe and confidential",
          "avoiding activities that may be illegal, weaken security, or harm other users",
          "using University information services for personal use",
          "reporting suspected incidents",
        ].map((item) => (
          <ListItem key={item}>
            <ListItemText primary={item} />
          </ListItem>
        ))}
      </List>
      <Typography variant="bodyLargeText" component="p" gutterBottom>
        {" "}
        <Link href={termsAndConditions} target="_blank" rel="noopener">
          Read the Acceptable Use Policy (link opens in a new window)
        </Link>
      </Typography>
      {/* Checkbox  with Validation */}
      <FormCheckbox<FormData>
        name="agreedToTerms"
        control={control}
        options={[
          { label: "I have read and agree to follow the Acceptable Use Policy", value: "yes" },
        ]}
      />
      {/* Navigation Buttons */}
      <NavigationButtons
        previousStep="userAccountDetails"
        previousLabel="Previous"
        nextLabel="Next"
      />
    </Container>
  );
};

export default TermsOfUse;
