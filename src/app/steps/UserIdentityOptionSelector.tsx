import { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import * as z from "zod";

// MUI components
import { Container, Link, Typography } from "@mui/material";

// Constants
import { guidanceOnAccountAndPasswords, identifiers } from "../consts";

// Custom components
import FormRadioGroup from "../components/forms/FormRadioGroup";
import NavigationButtons from "../components/NavigationButtons";

// Custom hooks
import useStore from "../store";

// Define the validation schema using Zod
const schema = z.object({
  hasCrsid: z
    .enum(["yes", "no"])
    .or(z.literal(""))
    .refine((val) => val !== "", {
      message: "Please select an option.",
    }),
});

// Infer the form data type from the schema
type FormData = z.infer<typeof schema>;

const UserIdentityOptionSelector = () => {
  const setIdentifier = useStore((state) => state.setIdentifier);
  const setStep = useStore((state) => state.setStep);

  const [initialIdentifier] = useState(() => useStore.getState().identifier);

  const defaultHasCrsid =
    initialIdentifier === identifiers.crsid
      ? "yes"
      : initialIdentifier === identifiers.lastName
        ? "no"
        : "";

  // Initialize react-hook-form with default values and validation resolver
  const { control, handleSubmit } = useForm<FormData>({
    resolver: zodResolver(schema),
    mode: "onBlur",
    defaultValues: {
      hasCrsid: defaultHasCrsid,
    },
  });

  // Handle form submission
  const onSubmit: SubmitHandler<FormData> = (data) => {
    if (data.hasCrsid === "yes") {
      setIdentifier(identifiers.crsid);
    } else if (data.hasCrsid === "no") {
      setIdentifier(identifiers.lastName);
    }
    setStep("userData");
  };

  return (
    <Container component="form" onSubmit={handleSubmit(onSubmit)} autoComplete="off">
      {/* Header */}
      <Typography variant="h3">Your University CRSid</Typography>
      <Typography variant="bodyLargeText" component="p">
        Your CRSid is a unique identifier used across University services and systems.
      </Typography>

      {/* Question */}
      <Typography variant="h4">Do you know your CRSid?</Typography>
      <Typography variant="body1">
        This is usually your initials followed by a numerical code. For example, ab1234 or dsb2222.
      </Typography>

      {/* Radio Buttons with Validation */}
      <FormRadioGroup<FormData>
        name="hasCrsid"
        control={control}
        options={[
          { label: "Yes, I know my CRSid", value: "yes" },
          { label: "No, I'll enter my last name instead", value: "no" },
        ]}
      />

      {/* Guidance Link */}
      <Typography variant="bodyLargeText" component="p" gutterBottom>
        {" "}
        <Link href={guidanceOnAccountAndPasswords} target="_blank" rel="noopener">
          Find out how to get a CRSid (opens in a new tab)
        </Link>
        {"."}
      </Typography>

      {/* Navigation Buttons */}
      <NavigationButtons previousStep="home" previousLabel="Previous" nextLabel="Next" />
    </Container>
  );
};

export default UserIdentityOptionSelector;
