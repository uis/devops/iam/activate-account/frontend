"use client";

// MUI components
import { Button, Container, Link, Typography } from "@mui/material";
import { serviceDeskEmail } from "../consts";

// Custom hooks
import useStore from "../store";

type Props = {
  clearError?: () => void;
};

const ErrorPage = ({ clearError }: Props) => (
  <Container>
    <Typography variant="h1">Sorry, there is a problem with this service</Typography>
    <Typography variant="body1" paragraph>
      Your progress has been lost and you will need to start again.
    </Typography>
    <Typography variant="body1" paragraph gutterBottom>
      If you are still experiencing problems contact{" "}
      <Link href={`mailto:${serviceDeskEmail}`}>[{serviceDeskEmail}]</Link>.
    </Typography>
    <Button
      variant="contained"
      onClick={() => {
        if (clearError) {
          clearError();
        }

        useStore.getState().setStep("home");
      }}
    >
      Try again
    </Button>
  </Container>
);

export default ErrorPage;
