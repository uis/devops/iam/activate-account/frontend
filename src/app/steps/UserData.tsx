import { zodResolver } from "@hookform/resolvers/zod";
import { useEffect, useMemo } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import * as z from "zod";

// Date utilities
import { isPast, isValid } from "date-fns";

// HTTP utilities
import { AxiosError } from "axios";

// MUI components
import { Box, Container, FormHelperText, Link, Stack, Typography } from "@mui/material";

// Custom hooks
import { useAuth } from "../authentication/AuthProvider";
import useStore from "../store";

// Custom components
import FormTextField from "../components/forms/FormTextField";
import NavigationButtons from "../components/NavigationButtons";

// Constants
import { identifiers, registrationProcessForStudents, steps } from "../consts";

const now = new Date();
const currentYear = now.getFullYear();

const dateSchema = z.object({
  date: z
    .object({
      day: z.coerce
        .number({ message: "Day must be a number" })
        .min(1, "Day must be at least 1")
        .max(31, "Day must be at most 31"),
      month: z.coerce
        .number({ message: "Month must be a number" })
        .min(1, "Month must be at least 1")
        .max(12, "Month must be at most 12"),
      year: z.coerce
        .number({ message: "Year must be a number" })
        .min(1900, "Year must be at least 1900")
        .max(currentYear, `Year must be at most ${currentYear}`),
    })
    .refine(({ year, month, day }) => {
      if (!year || !month || !day) {
        // Skip validation if any field is missing
        return true;
      }

      const date = new Date(year, month - 1, day);
      return (
        isValid(date) &&
        date.getFullYear() === year &&
        date.getMonth() === month - 1 &&
        date.getDate() === day
      );
    }, "Invalid date")
    .refine(({ year, month, day }) => {
      if (!year || !month || !day) {
        // Skip validation if any field is missing
        return true;
      }

      return isPast(new Date(year, month - 1, day));
    }, "Date cannot be in the future"),
});

const baseSchema = dateSchema.and(
  z.object({
    code: z
      .string()
      .min(1, "Enter your registration or admissions code")
      .max(20, "Registration or admissions code must contain at most 20 characters"),
  }),
);

const crsidSchema = baseSchema.and(
  z.object({
    crsid: z
      .string()
      .min(1, "Enter your CRSid")
      .max(20, "CRSid must contain at most 20 characters"),
  }),
);

const lastNameSchema = baseSchema.and(
  z.object({
    lastName: z
      .string()
      .min(1, "Enter your last name")
      .max(100, "Last name must contain at most 100 characters"),
  }),
);

type FormData = z.infer<typeof crsidSchema> | z.infer<typeof lastNameSchema>;

const identifierProps = {
  [identifiers.crsid]: {
    label: "CRSid",
    name: "crsid",
    schema: crsidSchema,
  },
  [identifiers.lastName]: {
    label: "Last name",
    name: "lastName",
    schema: lastNameSchema,
  },
} as const;

const UserData = () => {
  const selectedIdentifier = useStore((state) => state.identifier);
  const setStep = useStore((state) => state.setStep);
  const identifier =
    identifierProps[selectedIdentifier as keyof typeof identifierProps] ||
    identifierProps[identifiers.crsid];

  const schema = useMemo(() => identifier.schema, [identifier]);
  const { authenticate, revoke } = useAuth();

  useEffect(() => {
    // Each time this step mounts, we want to clear any access token that we previously had.
    revoke();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<FormData>({
    resolver: zodResolver(schema),
    mode: "onBlur",
    defaultValues: {
      crsid: "",
      lastName: "",
      date: {
        // The field type is `text`, but the schema expects a number. We want the field to be
        // empty, but using a number (such as 0) will display that number. We also can't set
        // `undefined` because then the field would be uncontrolled. So we have to work around
        // this by type casting.
        day: "" as unknown as number,
        month: "" as unknown as number,
        year: "" as unknown as number,
      },
      code: "",
    },
  });

  const onSubmit: SubmitHandler<FormData> = (formData) =>
    authenticate({
      dateOfBirth: `${formData.date.year}-${formData.date.month}-${formData.date.day}`,
      code: formData.code,
      crsid: "crsid" in formData ? formData.crsid : undefined,
      lastName: "lastName" in formData ? formData.lastName : undefined,
    })
      .then(() => {
        setStep(steps.userAccountDetails);
      })
      .catch((error) => {
        if (
          error instanceof AxiosError &&
          error.response?.status === 400 &&
          error.response?.data?.error === "invalid_grant"
        ) {
          setStep(steps.universityAccountNotFound);
        }
      });

  return (
    <Container component="form" onSubmit={handleSubmit(onSubmit)} autoComplete="off">
      <Typography variant="h3">Enter your University account details</Typography>
      <Typography variant="bodyLargeText" component="p">
        You need to provide your University account details, so that we can verify your identity.
      </Typography>

      {/* Identifier */}
      <FormTextField name={identifier.name} control={control} label={identifier.label} />

      {/* Date of birth */}
      <Typography variant="h4">Date of birth</Typography>
      <Typography variant="bodyLargeText" component="p" gutterBottom>
        For example, 27 03 2001
      </Typography>
      <Stack direction="row" spacing={2}>
        <Box>
          <FormTextField
            name="date.day"
            label="Day"
            control={control}
            inputMode="numeric"
            helperText={null} // Explicitly disable helper text
            error={!!errors.date?.root || !!errors.date?.day}
            slotProps={{
              inputLabel: {
                style: {
                  marginTop: 0,
                },
              },
              htmlInput: {
                maxLength: 2,
                pattern: "[0-9]*",
                style: {
                  width: "5ch",
                },
              },
            }}
          />
        </Box>
        <Box>
          <FormTextField
            name="date.month"
            label="Month"
            control={control}
            inputMode="numeric"
            helperText={null} // Explicitly disable helper text
            error={!!errors.date?.root || !!errors.date?.month}
            slotProps={{
              inputLabel: {
                style: {
                  marginTop: 0,
                },
              },
              htmlInput: {
                maxLength: 2,
                pattern: "[0-9]*",
                style: {
                  width: "5ch",
                },
              },
            }}
          />
        </Box>
        <Box>
          <FormTextField
            name="date.year"
            label="Year"
            control={control}
            inputMode="numeric"
            helperText={null} // Explicitly disable helper text
            error={!!errors.date?.root || !!errors.date?.year}
            slotProps={{
              inputLabel: {
                style: {
                  marginTop: 0,
                },
              },
              htmlInput: {
                maxLength: 4,
                pattern: "[0-9]*",
                style: {
                  width: "6ch",
                },
              },
            }}
          />
        </Box>
      </Stack>
      {errors.date
        ? (["day", "month", "year", "root"] as const)
            .filter((attribute) => attribute in errors.date!)
            .map((attribute) => (
              <FormHelperText key={attribute} error>
                {errors.date![attribute]!.message}
              </FormHelperText>
            ))
        : null}

      {/* Registration code or admissions number */}

      <FormTextField
        name="code"
        control={control}
        label="Registration code or admissions number"
        description={
          <>
            <Typography variant="bodyLargeText" component="p" gutterBottom>
              If you are a member of staff, your institution or line manager will provide you with
              a registration code.
            </Typography>
            <Typography variant="bodyLargeText" component="p" gutterBottom>
              If you are a student, you will have been given an admissions number. This can be
              different depending on your type of study or institution. The{" "}
              <Link href={registrationProcessForStudents} target="_blank">
                Registration process for students
              </Link>{" "}
              provides a list of admissions numbers.
            </Typography>
          </>
        }
      />

      {/* Get help with your code */}
      <Typography variant="h4">Get help with your code</Typography>
      <Typography variant="bodyLargeText" component="p" gutterBottom>
        If you are a member of staff, contact your line manager, local HR or IT team, if you
        haven&apos;t received your code or can&apos;t find it.
      </Typography>
      <Typography variant="bodyLargeText" component="p" gutterBottom>
        If you are a student, contact your College (Undergraduate students) or your Institution
        (Postgraduate students) if you haven&apos;t received this or can&apos;t find it.
      </Typography>

      {/* Navigation Buttons */}
      <NavigationButtons previousStep="identity" previousLabel="Previous" nextLabel="Next" />
    </Container>
  );
};

export default UserData;
