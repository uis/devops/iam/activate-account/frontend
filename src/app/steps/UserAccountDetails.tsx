import { zodResolver } from "@hookform/resolvers/zod";
import { Account } from "activate-account";
import { useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import * as z from "zod";

// MUI components
import {
  Box,
  Card,
  CircularProgress,
  Container,
  Link,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";
import Grid from "@mui/material/Grid2";

// Custom components
import FormRadioGroup from "../components/forms/FormRadioGroup";
import NavigationButtons from "../components/NavigationButtons";

// Custom hooks
import { useAuth } from "../authentication/AuthProvider";
import useStore from "../store";

// Constants
import { steps, userAdminEmail } from "../consts";

const displayDetails: {
  displayName: string;
  property: keyof Account;
}[] = [
  { displayName: "Name", property: "name" },
  { displayName: "College", property: "college" },
  { displayName: "Affiliation", property: "affiliation" },
];

// Define the validation schema using Zod
const schema = z.object({
  informationCorrect: z.enum(["yes", "no", ""]).refine((val) => val !== "", {
    message: "Please select an option.",
  }),
});

type FormData = z.infer<typeof schema>;

const UserAccountDetails = () => {
  const { client } = useAuth();
  const setStep = useStore((state) => state.setStep);

  const [accountDetails, setAccountDetails] = useState<Account | null>(null);

  useEffect(() => {
    async function retrieveDetails() {
      const data = (await client.getAccount()).data;
      setAccountDetails(data);
    }
    retrieveDetails();
  }, [client]);

  // Initialize react-hook-form with default values and validation resolver
  const { control, handleSubmit, watch } = useForm<FormData>({
    resolver: zodResolver(schema),
    mode: "onBlur",
    defaultValues: {
      informationCorrect: "",
    },
  });

  const informationCorrect = watch("informationCorrect");

  // Handle form submission
  const onSubmit: SubmitHandler<FormData> = (data) => {
    if (data.informationCorrect === "yes") {
      setStep(steps.terms);
    } else if (data.informationCorrect === "no") {
      setStep(steps.home);
    }
  };

  return (
    <Container component="form" onSubmit={handleSubmit(onSubmit)} autoComplete="off">
      {/* Header */}
      <Typography variant="h3">Confirm your details</Typography>
      <Typography variant="bodyLargeText" component="p">
        The University account details you have provided match the following profile.
      </Typography>

      <Card variant="outlined" className="PaddedContent">
        {accountDetails === null ? (
          <CircularProgress />
        ) : (
          <Grid container columns={6} rowSpacing="12px">
            {displayDetails.map(({ displayName, property }) => (
              <Grid container size={6} key={displayName}>
                <Grid size={6}>
                  <Typography variant="bodyLargeText" component="p">
                    {accountDetails[property]}
                  </Typography>
                </Grid>
              </Grid>
            ))}
          </Grid>
        )}
      </Card>

      {/* Question */}
      <Typography variant="h4">Is this information correct?</Typography>

      {/* Radio Buttons with Validation */}
      <FormRadioGroup<FormData>
        name="informationCorrect"
        control={control}
        options={[
          { label: "Yes, these details are correct", value: "yes" },
          { label: "No, something is wrong", value: "no" },
        ]}
      />
      {/* Conditional display of additional instructions */}
      {informationCorrect === "no" && (
        <Box pt={4}>
          <Typography variant="bodyLargeText" component="p" gutterBottom>
            Contact <Link href={`mailto:${userAdminEmail}`}>{userAdminEmail}</Link> if this
            information is incorrect.
          </Typography>
          <Typography variant="bodyLargeText" component="p">
            Include your:
          </Typography>

          <List className="MuiList-unordered">
            {["CRSid (if known)", "full name", "date of birth"].map((item) => (
              <ListItem key={item}>
                <ListItemText primary={item} />
              </ListItem>
            ))}
          </List>
        </Box>
      )}

      {/* Navigation Buttons */}
      <NavigationButtons
        previousStep="userData"
        previousLabel="Previous"
        nextLabel={informationCorrect === "no" ? "Finish" : "Next"}
      />
    </Container>
  );
};

export default UserAccountDetails;
