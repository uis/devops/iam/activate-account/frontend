"use client";

import { useEffect } from "react";
import { useRouter } from "next/navigation";
import { Box, CssBaseline } from "@mui/material";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Stepper from "./components/Stepper";
import { LoggerProvider } from "./logger/LoggerProvider";

import { initGoogleAnalytics } from "@ucam.uis.devops/ucam-observe-remote-analytics";

import ErrorBoundary from "./components/ErrorBoundary";
import { useConfig } from "./config/ConfigProvider";

export default function Home() {
  const router = useRouter();
  const config = useConfig();

  useEffect(() => {
    const { pathname, search } = window.location;
    if (pathname !== `${config.HOSTED_PATH}/` || search) {
      router.replace("/");
    }
  }, [router, config.HOSTED_PATH]);

  useEffect(() => {
    try {
      initGoogleAnalytics(config.GA_TRACKING_ID);
    } catch (e) {}
  }, [config]);

  return (
    <LoggerProvider>
      <CssBaseline />
      <main>
        <Box sx={{ flexGrow: 1 }}>
          <Header />
          <ErrorBoundary>
            <Stepper />
          </ErrorBoundary>
          <Footer />
        </Box>
      </main>
    </LoggerProvider>
  );
}
