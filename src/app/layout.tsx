import { AppRouterCacheProvider } from "@mui/material-nextjs/v13-appRouter";
import { ThemeProvider } from "@mui/material/styles";
import type { Metadata } from "next";
import theme from "../styles/theme";
import CookieBannerWrapper from "./components/CookieBannerWrapper";
import { ConfigProvider } from "./config/ConfigProvider";

export const metadata: Metadata = {
  title: "Get Your University IT Account",
  description: "",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body>
        <AppRouterCacheProvider>
          <ConfigProvider>
            <ThemeProvider theme={theme}>
              <CookieBannerWrapper />
              {children}
            </ThemeProvider>
          </ConfigProvider>
        </AppRouterCacheProvider>
      </body>
    </html>
  );
}
