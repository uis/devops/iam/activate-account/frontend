"use client";

// React imports
import { ComponentType, createElement } from "react";

// Custom hooks
import useStore from "../store";

// Custom providers
import AuthProvider from "../authentication/AuthProvider";
import ReCaptchaProvider from "../captcha/ReCaptchaProvider";

// Constants
import { steps } from "../consts";

// Custom components (steps)
import SessionExpired from "../steps/SessionExpired";
import TermsOfUse from "../steps/TermsOfUse";
import UniversityAccountRegistration from "../steps/UniversityAccountRegistration";
import UserData from "../steps/UserData";
import UserIdentityOptionSelector from "../steps/UserIdentityOptionSelector";
import UserAccountDetails from "../steps/UserAccountDetails";
import UniversityAccountNotFound from "../steps/UniversityAccountNotFound";
import UserAccountOverview from "../steps/UserAccountOverview";
import ErrorPage from "../steps/ErrorPage";

type Components = {
  [key in (typeof steps)[keyof typeof steps]]: { component: ComponentType; authContext?: boolean };
};

const components: Components = {
  [steps.home]: {
    component: UniversityAccountRegistration,
  },
  [steps.identity]: {
    component: UserIdentityOptionSelector,
  },
  [steps.userData]: {
    component: UserData,
    authContext: true,
  },
  [steps.userAccountDetails]: {
    component: UserAccountDetails,
    authContext: true,
  },
  [steps.sessionExpired]: {
    component: SessionExpired,
  },
  [steps.universityAccountNotFound]: {
    component: UniversityAccountNotFound,
  },
  [steps.terms]: {
    component: TermsOfUse,
    authContext: true,
  },
  [steps.accountOverview]: {
    component: UserAccountOverview,
    authContext: true,
  },
  [steps.error]: {
    component: ErrorPage,
  },
};

const Stepper = () => {
  const step = useStore((state) => state.step);
  const { component, authContext: auth = false } = components[step];
  const element = createElement(component);

  // Switching from a page that needs the `authContext` to a page without it
  // will destroy the provider, which is what we want so that we destroy the
  // access token.
  // The AuthProvider and the ReCaptchaProvider work together, we want to re-
  // request a recaptcha token before running auth.
  return auth ? (
    <ReCaptchaProvider>
      <AuthProvider>{element}</AuthProvider>
    </ReCaptchaProvider>
  ) : (
    element
  );
};

export default Stepper;
