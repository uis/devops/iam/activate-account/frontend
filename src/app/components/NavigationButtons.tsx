// React imports
import { FC, ReactNode } from "react";

// MUI components
import { Button, Box } from "@mui/material";

// Custom hooks
import useStore from "../store";

// Types
import { Steps } from "../types";

interface NavigationButtonsProps {
  previousStep: Steps;
  nextStep?: Steps;
  previousLabel?: ReactNode;
  nextLabel?: ReactNode;
}

const NavigationButtons: FC<NavigationButtonsProps> = ({
  previousStep,
  nextStep,
  previousLabel = "Previous",
  nextLabel = "Next",
}) => {
  const { setStep } = useStore();

  return (
    <Box paddingTop="60px" display="flex" gap={2}>
      <Button variant="contained" color="secondary" onClick={() => setStep(previousStep)}>
        {previousLabel}
      </Button>
      <Button
        variant="contained"
        color="primary"
        onClick={() => (nextStep ? setStep(nextStep) : undefined)}
        type="submit"
      >
        {nextLabel}
      </Button>
    </Box>
  );
};

export default NavigationButtons;
