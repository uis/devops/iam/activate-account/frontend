"use client";

// MUI components
import { AppBar, Toolbar } from "@mui/material";

// Next.js components
import Image from "next/image";
import { useConfig } from "../config/ConfigProvider";

const Header = () => {
  const config = useConfig();

  return (
    <AppBar position="static" variant="headerBox">
      <Toolbar>
        <Image
          width={160}
          height={34}
          src={`${config.HOSTED_PATH}/cambridge-logo.png`}
          alt="University of Cambridge"
        />
      </Toolbar>
    </AppBar>
  );
};

export default Header;
