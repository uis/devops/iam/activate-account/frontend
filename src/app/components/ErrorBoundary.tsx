import React, { ContextType, ReactNode } from "react";
import { LoggerContext } from "../logger/LoggerProvider";
import ErrorPage from "../steps/ErrorPage";

interface ErrorBoundaryProps {
  children: ReactNode;
}

interface ErrorBoundaryState {
  hasError: boolean;
}

class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
  static contextType = LoggerContext;
  context!: ContextType<typeof LoggerContext>;

  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(): ErrorBoundaryState {
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    if (this.context?.logger) {
      this.context.logger.error("Uncaught error:", error, errorInfo);
    } else {
      console.error("Logger is not available:", error, errorInfo);
    }
  }

  clearError() {
    this.setState({ hasError: false });
  }

  render() {
    if (this.state.hasError) {
      return <ErrorPage clearError={() => this.clearError()} />;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
