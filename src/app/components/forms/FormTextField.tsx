import { InputLabel, TextField } from "@mui/material";
import { ComponentProps, ReactNode, useId } from "react";
import { Controller, FieldValues } from "react-hook-form";

type Props<TFieldValues extends FieldValues = FieldValues> = Pick<
  ComponentProps<typeof Controller<TFieldValues>>,
  "name" | "control"
> &
  ComponentProps<typeof TextField> & {
    description?: ReactNode;
  };

const FormTextField = <TFieldValues extends FieldValues = FieldValues>({
  name,
  control,
  label,
  description,
  slotProps = {},
  ...props
}: Props<TFieldValues>) => {
  const id = useId();
  const inputLabelId = label && id ? `${id}-label` : undefined;

  const fieldProps = description
    ? {
        slotProps: {
          input: {
            id,
            ...slotProps?.input,
          },
          ...slotProps,
        },
      }
    : {
        label,
        slotProps,
      };

  return (
    <Controller<TFieldValues>
      name={name}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <>
          {description && label ? (
            <InputLabel error={!!error} htmlFor={id} id={inputLabelId}>
              {label}
            </InputLabel>
          ) : null}
          {description}
          <TextField
            helperText={error ? error.message : null}
            error={!!error}
            size="small"
            {...fieldProps}
            {...field}
            {...props}
          />
        </>
      )}
    />
  );
};

export default FormTextField;
