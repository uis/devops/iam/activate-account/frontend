import { Checkbox, FormControl, FormControlLabel, FormHelperText, FormLabel } from "@mui/material";
import { ComponentProps, ReactNode, useId } from "react";
import { Controller, FieldValues } from "react-hook-form";

type Option = {
  label: string;
  value: string;
};

type Props<TFieldValues extends FieldValues = FieldValues> = Pick<
  ComponentProps<typeof Controller<TFieldValues>>,
  "name" | "control"
> & {
  label?: string;
  options: Option[];
  description?: ReactNode;
  formControlProps?: ComponentProps<typeof FormControl>;
};

const FormCheckbox = <TFieldValues extends FieldValues>({
  name,
  control,
  label,
  options,
  description,
  formControlProps,
}: Props<TFieldValues>) => {
  const id = useId();
  const ariaLabelledBy = `${id}-label`;

  return (
    <Controller<TFieldValues>
      name={name}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <>
          {label ? (
            <FormLabel component="legend" id={ariaLabelledBy}>
              {label}
            </FormLabel>
          ) : null}
          <FormControl error={!!error} margin="normal" fullWidth {...formControlProps}>
            {description && <FormHelperText component="div">{description}</FormHelperText>}
            {options.map((option) => (
              <FormControlLabel
                key={option.value}
                control={
                  <Checkbox
                    {...field}
                    checked={
                      Array.isArray(field.value) ? field.value.includes(option.value) : false
                    }
                    onChange={(e) => {
                      const newValue = e.target.checked
                        ? [...(field.value || []), option.value]
                        : field.value.filter((val: string) => val !== option.value);
                      field.onChange(newValue);
                    }}
                  />
                }
                label={option.label}
              />
            ))}
            {error && <FormHelperText>{error.message}</FormHelperText>}
          </FormControl>
        </>
      )}
    />
  );
};

export default FormCheckbox;
