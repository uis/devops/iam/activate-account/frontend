import {
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import { ComponentProps, ReactNode, useId } from "react";
import { Controller, FieldValues } from "react-hook-form";

type Option = {
  label: string;
  value: string;
};

type Props<TFieldValues extends FieldValues = FieldValues> = Pick<
  ComponentProps<typeof Controller<TFieldValues>>,
  "name" | "control"
> & {
  label?: string;
  options: Option[];
  description?: ReactNode;
  formControlProps?: ComponentProps<typeof FormControl>;
};

const FormRadioGroup = <TFieldValues extends FieldValues>({
  name,
  control,
  label,
  options,
  description,
  formControlProps,
}: Props<TFieldValues>) => {
  const id = useId();
  const ariaLabelledBy = `${id}-label`;

  return (
    <Controller<TFieldValues>
      name={name}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <>
          {label ? (
            <FormLabel component="legend" id={ariaLabelledBy}>
              {label}
            </FormLabel>
          ) : null}
          <FormControl error={!!error} margin="normal" fullWidth {...formControlProps}>
            {description && <FormHelperText component="div">{description}</FormHelperText>}
            <RadioGroup aria-labelledby={ariaLabelledBy} {...field}>
              {options.map((option) => (
                <FormControlLabel
                  key={option.value}
                  value={option.value}
                  control={<Radio />}
                  label={option.label}
                />
              ))}
            </RadioGroup>
            {error && <FormHelperText>{error.message}</FormHelperText>}
          </FormControl>
        </>
      )}
    />
  );
};

export default FormRadioGroup;
