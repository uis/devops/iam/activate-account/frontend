"use client";

import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import log from "loglevel";
import { createContext, ReactNode, useContext, useEffect, useState } from "react";

interface AppConfig {
  API_BASE_PATH: string;

  UCAM_OBSERVE_REMOTE_SERVER_BASE_URL: string;
  UCAM_OBSERVE_REMOTE_SERVER_CLIENT_ID: string;
  GA_TRACKING_ID: string;

  RECAPTCHA_KEY: string;

  TOKEN_REDIRECT_URL: string;

  HOSTED_PATH: string;
}

const ConfigContext = createContext<AppConfig | null>(null);

interface ConfigProviderProps {
  children: ReactNode;
}

let cachedConfig: AppConfig | null = null;

export const ConfigProvider = ({ children }: ConfigProviderProps) => {
  const [config, setConfig] = useState<AppConfig | null>(cachedConfig);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(!cachedConfig);

  useEffect(() => {
    const fetchConfig = async () => {
      if (!cachedConfig) {
        try {
          setLoading(true);
          // Try loading config.json from the current path. In production, nginx will ensure that
          // the file is served from the public folder
          const configURI = `${window.location.origin}${window.location.pathname}/config.json`;
          const response = await fetch(configURI);
          const config: AppConfig = await response.json();
          setConfig(config);
          cachedConfig = config;
        } catch (err) {
          log.error("Error loading config:", err);
          setError("Failed to load configuration");
        } finally {
          setLoading(false);
        }
      }
    };

    fetchConfig();
  }, []);

  if (loading) {
    return (
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <CircularProgress />
      </Box>
    );
  }

  if (error || !config) {
    log.error(error);
    return <div>Error loading configuration</div>;
  }

  return <ConfigContext.Provider value={config}>{children}</ConfigContext.Provider>;
};

export const useConfig = () => {
  const context = useContext(ConfigContext);
  if (!context) {
    throw new Error("useConfig must be used within a ConfigProvider");
  }
  return context;
};
