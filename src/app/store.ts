import type {} from "@redux-devtools/extension"; // required for devtools typing
import { create } from "zustand";
import { devtools } from "zustand/middleware";
import { Identifiers, Steps } from "./types";

type State = {
  step: Steps;
  setStep(step: Steps): void;

  identifier: Identifiers;
  setIdentifier(identifier: Identifiers): void;
};

const useStore = create<State>()(
  devtools((set) => ({
    step: "home",
    setStep: (step) => {
      set({
        step,
      });
    },

    identifier: null,
    setIdentifier: (identifier) => {
      set({
        identifier,
      });
    },
  })),
);

export default useStore;
