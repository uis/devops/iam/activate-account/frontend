"use client";

import { createContext, ReactNode, useContext, useEffect, useMemo } from "react";
import { getPinoRemoteLogger, setupGlobalErrorHandler } from "ucam-observe-remote-client";
import { useConfig } from "../config/ConfigProvider";

interface LoggerContextType {
  logger: ReturnType<typeof getPinoRemoteLogger> | null;
}

export const LoggerContext = createContext<LoggerContextType | null>(null);

interface LoggerProviderProps {
  children: ReactNode;
}

export const LoggerProvider = ({ children }: LoggerProviderProps) => {
  const config = useConfig();
  const logger = useMemo(
    () =>
      config
        ? getPinoRemoteLogger({
            level: "info",
            clientId: config.UCAM_OBSERVE_REMOTE_SERVER_CLIENT_ID,
            remoteUrl: `${config.UCAM_OBSERVE_REMOTE_SERVER_BASE_URL}/log`,
          })
        : null,
    [config],
  );

  useEffect(() => {
    if (!logger) {
      return;
    }

    return setupGlobalErrorHandler(logger);
  }, [logger]);

  return <LoggerContext.Provider value={{ logger }}>{children}</LoggerContext.Provider>;
};

export const useLogger = () => {
  const context = useContext(LoggerContext);
  if (!context) {
    throw new Error("useLogger must be used within a LoggerProvider");
  }
  return context.logger;
};
