export const checkAccessibility = (options = {}, ignoreRules = []) => {
  cy.injectAxe();
  cy.checkA11y(
    undefined,
    {
      ...options,
      rules: {
        ...ignoreRules.reduce(
          (acc, rule) => ({
            ...acc,
            [rule]: { enabled: false },
          }),
          {},
        ),
      },
    },
    (violations) => {
      violations.forEach((violation) => {
        const nodes = violation.nodes.map((node) => node.target).join(", ");
        cy.log(`Accessibility violation: ${violation.help}`);
        cy.log(`Description: ${violation.description}`);
        cy.log(`Impact: ${violation.impact}`);
        cy.log(`Nodes: ${nodes}`);
        cy.log(`Help URL: ${violation.helpUrl}`);
      });
      if (violations.length) {
        const violationMessages = violations
          .map((v) => {
            const nodeTargets = v.nodes.map((n) => n.target.join(", ")).join("; ");
            return `${v.help} (Impact: ${v.impact})\nNodes: ${nodeTargets}\nHelp URL: ${v.helpUrl}`;
          })
          .join("\n\n");
        assert.fail(`Accessibility violations detected:\n\n${violationMessages}`);
      }
    },
  );
};
