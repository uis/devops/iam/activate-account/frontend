declare namespace Cypress {
  interface Chainable<Subject> {
    /**
     * Custom command to log accessibility violations.
     * @example cy.logViolation(violation)
     */
    logViolation(violation: any): Chainable<any>;
  }
}
interface AxeNode {
  target: string[];
}
interface AxeViolation {
  help: string;
  description: string;
  impact: string;
  nodes: AxeNode[];
  helpUrl: string;
}
declare namespace Cypress {
  interface Chainable<Subject> {
    logViolation(violation: AxeViolation): Chainable<any>;
  }
}
