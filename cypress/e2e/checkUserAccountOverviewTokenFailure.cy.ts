import { checkAccessibility } from "../support/accessibilityTests";
import { API_BASE_URL, fillDetails, setStandardIntercepts } from "./utilities";

describe("UserAccountOverviewTokenFailure Component", () => {
  beforeEach(() => {
    // Setup API intercepts
    setStandardIntercepts();

    // Override the reset token response to be a 400
    cy.intercept("GET", `${API_BASE_URL}reset-token/`, {
      statusCode: 400,
      body: { crsid: "User not found" },
    }).as("getResetToken");

    // Navigate to page
    cy.visit("/", {
      onBeforeLoad: (win) => {
        cy.stub(win, "alert").as("alert");
      },
    });

    cy.window().then((win) => {
      if (win.navigator.clipboard) {
        cy.stub(win.navigator.clipboard, "writeText").as("writeTextStub");
      } else {
        win.navigator.clipboard = {
          writeText: cy.stub().as("writeTextStub"),
        };
      }
    });
    cy.get("#modal-scroll-button").click();
    cy.get("#dismiss-banner-button").click();
    cy.get("button").contains("Get your University IT account sign in details").click();
    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();
    fillDetails();
    cy.get('button[type="submit"]').click();
    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();
    cy.contains("Review and agree to the Acceptable Use Policy").should("be.visible");
    cy.get('[type="checkbox"]').check({ force: true });
    cy.get('button[type="submit"]').click();
    cy.contains("University account not found").should("be.visible");
  });

  describe("check general accessibility and navigation", () => {
    it("should be accessible", () => {
      checkAccessibility({}, ["heading-order", "page-has-heading-one"]);
    });

    it("goes to the previous page when clicking 'Previous'", () => {
      cy.get("button").contains("Previous").click();
      cy.contains("Enter your University account details").should("be.visible");
    });
    it("goes to the previous page when clicking 'Re-enter your account details'", () => {
      cy.get("button").contains("Re-enter your account details").click();
      cy.contains("Enter your University account details").should("be.visible");
    });
  });
});
