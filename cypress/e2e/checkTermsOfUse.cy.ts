import { checkAccessibility } from "../support/accessibilityTests";
import { API_BASE_URL, fillDetails, setStandardIntercepts } from "./utilities";

describe("TermsOfUse Component", () => {
  beforeEach(() => {
    // Setup API intercepts
    setStandardIntercepts();

    // Navigate to page
    cy.visit("/", {
      onBeforeLoad: (win) => {
        cy.stub(win, "alert").as("alert");
      },
    });
    cy.get("#modal-scroll-button").click();
    cy.get("#dismiss-banner-button").click();
    cy.get("button").contains("Get your University IT account sign in details").click();
    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();
    fillDetails();
    cy.get('button[type="submit"]').click();
    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();
    cy.contains("Review and agree to the Acceptable Use Policy").should("be.visible");
  });

  describe("Check general accessibility and navigation", () => {
    it("should be accessible", () => {
      checkAccessibility({}, ["heading-order", "page-has-heading-one"]);
    });

    it("goes to the previous page when clicking 'Previous'", () => {
      cy.get("button").contains("Previous").click();
      cy.contains("Confirm your details").should("be.visible");
    });
  });

  describe("Submission", () => {
    it("should display a validation message if trying to submit without selection", () => {
      cy.get('button[type="submit"]').click();
      cy.contains("⚠ You must agree to the terms of use").should("be.visible");
    });

    it("should go to the next page when the checkbox is checked", () => {
      cy.get('[type="checkbox"]').check({ force: true });
      cy.get('button[type="submit"]').click();
      cy.contains("Account verified").should("be.visible");
    });

    [404, 500].forEach((statusCode) => {
      it(`Server error ${statusCode}`, () => {
        cy.intercept("PATCH", `${API_BASE_URL}account/`, { statusCode }).as("patchAccountDetails");

        cy.get('[type="checkbox"]').check({ force: true });
        cy.get('button[type="submit"]').click();
        cy.contains("Sorry, there is a problem with this service").should("be.visible");
        cy.wait("@postLog").its("response.statusCode").should("eq", 200);
      });
    });
  });
});
