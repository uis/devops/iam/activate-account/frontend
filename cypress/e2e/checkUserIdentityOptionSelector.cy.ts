import { checkAccessibility } from "../support/accessibilityTests";

describe("UserIdentityOptionSelector", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.get("#modal-scroll-button").click();
    cy.get("#dismiss-banner-button").click();
    cy.get("button").contains("Get your University IT account sign in details").click();
    cy.contains("Your University CRSid").should("be.visible");
  });

  it("should be accessible", () => {
    checkAccessibility({}, ["heading-order", "page-has-heading-one"]);
  });

  it("should display the correct default state", () => {
    cy.get('input[name="hasCrsid"]').should("not.be.checked");
  });

  it('should allow selecting "yes" and submit the form', () => {
    cy.get('input[value="yes"]').click();
    cy.get('button[type="submit"]').click();
    cy.contains("CRSid").should("be.visible");
  });

  it('should allow selecting "no" and submit the form', () => {
    cy.get('input[value="no"]').click();
    cy.get('button[type="submit"]').click();
    cy.contains("Last name").should("be.visible");
  });

  it("should display a validation message if trying to submit without selection", () => {
    cy.get('button[type="submit"]').click();
    cy.contains("Please select an option.").should("be.visible");
  });

  it("should retain the selected value when navigating back to the form", () => {
    cy.get('input[value="yes"]').click();
    cy.get('button[type="submit"]').click();
    cy.contains("Enter your University account details").should("be.visible");

    cy.get("button").contains("Previous").click();
    cy.contains("Your University CRSid").should("be.visible");

    cy.get('input[value="yes"]').should("be.checked");
  });
});
