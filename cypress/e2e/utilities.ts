export const fillDetails = () => {
  const details = {
    crsid: "ab1234",
    "date.day": "27",
    "date.month": "3",
    "date.year": "2001",
    code: "123456",
  } as const;

  Object.entries(details).forEach(([name, value]) => {
    cy.get(`[name="${name}"]`).type(value);
  });
};

type Opts = {
  apiBaseUrl?: string;
  tokenBaseUrl?: string;
  ucamObserveRemoteServerLogUrl?: string;
};

export const API_BASE_URL = `${Cypress.env("API_BASE_PATH")}/v1alpha1/`;
export const TOKEN_BASE_URL = `${Cypress.env("API_BASE_PATH")}/token/`;
export const UCAM_OBSERVE_REMOTE_SERVER_LOG_URL = `${Cypress.env("UCAM_OBSERVE_REMOTE_SERVER_BASE_URL")}/log`;

export const setStandardIntercepts = ({
  apiBaseUrl = API_BASE_URL,
  tokenBaseUrl = TOKEN_BASE_URL,
  ucamObserveRemoteServerLogUrl = UCAM_OBSERVE_REMOTE_SERVER_LOG_URL,
}: Opts = {}) => {
  cy.intercept("POST", ucamObserveRemoteServerLogUrl, {
    statusCode: 200,
    body: {},
  }).as("postLog");
  cy.intercept("POST", tokenBaseUrl, {
    statusCode: 200,
    body: { access_token: "foo", expires_in: 60 * 60 * 5 },
  }).as("getToken");
  cy.intercept("POST", `${tokenBaseUrl}/revoke/`, {
    statusCode: 204,
  }).as("revokeToken");
  cy.intercept("GET", `${apiBaseUrl}account/`, {
    statusCode: 200,
    body: {
      crsid: "ab1234",
      name: "Test User",
      college: "Test College",
      affiliation: "Test Institution",
    },
  }).as("getAccountDetails");
  cy.intercept("PATCH", `${apiBaseUrl}account/`, {
    statusCode: 200,
    body: {
      crsid: "ab1234",
      name: "Test User",
      college: "Test College",
      affiliation: "Test Institution",
      termsAccepted: true,
    },
  }).as("patchAccountDetails");
  cy.intercept("GET", `${apiBaseUrl}reset-token/`, {
    statusCode: 200,
    body: {
      token: "TEST-TEST-TEST-TEST",
    },
  }).as("getResetToken");
};
