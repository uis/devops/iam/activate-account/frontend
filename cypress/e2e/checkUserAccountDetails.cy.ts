import { checkAccessibility } from "../support/accessibilityTests";
import { fillDetails, setStandardIntercepts } from "./utilities";

describe("UserAccountDetails Component", () => {
  beforeEach(() => {
    // Setup API intercepts
    setStandardIntercepts();

    // Navigate to page
    cy.visit("/", {});
    cy.get("#modal-scroll-button").click();
    cy.get("#dismiss-banner-button").click();
    cy.get("button").contains("Get your University IT account sign in details").click();
    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();
    fillDetails();
    cy.get('button[type="submit"]').click();
    cy.contains("Confirm your details").should("be.visible");
  });

  describe("Check general accessibility and navigation", () => {
    it("should be accessible", () => {
      checkAccessibility({}, ["heading-order", "page-has-heading-one"]);
    });

    it("goes to the previous page when clicking 'Previous'", () => {
      cy.get("button").contains("Previous").click();
      cy.contains("Enter your University account details").should("be.visible");
    });
  });

  describe("Check data is displayed", () => {
    it("should display the account details", () => {
      cy.contains("Test User").should("be.visible");
      cy.contains("Test College").should("be.visible");
      cy.contains("Test Institution").should("be.visible");
    });
  });

  describe("Submission", () => {
    it("should display a validation message if trying to submit without selection", () => {
      cy.get('button[type="submit"]').click();
      cy.contains("Please select an option.").should("be.visible");
    });

    it("should display additional info if 'no' is selected", () => {
      cy.get(`input[value='no']`).first().check();
      cy.contains("Include your:").should("be.visible");
      cy.contains("Finish").should("be.visible");
      cy.get('button[type="submit"]').click();
      cy.contains("Get your University IT account sign in details").should("be.visible");
    });

    it("should navigate to the next page 'yes' is selected", () => {
      cy.get(`input[value='yes']`).first().check();
      cy.contains("Next").should("be.visible");
      cy.get('button[type="submit"]').click();
      cy.contains("Review and agree to the Acceptable Use Policy").should("be.visible");
    });
  });
});

export {};
