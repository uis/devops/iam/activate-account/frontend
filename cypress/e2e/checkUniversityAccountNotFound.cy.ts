import { checkAccessibility } from "../support/accessibilityTests";
import { fillDetails, setStandardIntercepts, TOKEN_BASE_URL } from "./utilities";

describe("UniversityAccountNotFound Component", () => {
  beforeEach(() => {
    setStandardIntercepts();
    cy.visit("/", {
      onBeforeLoad: (win) => {
        cy.stub(win, "alert").as("alert");
      },
    });
    cy.get("#modal-scroll-button").click();
    cy.get("#dismiss-banner-button").click();
    cy.get("button").contains("Get your University IT account sign in details").click();

    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();
    cy.get("button").contains("Previous").click();
    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();

    cy.intercept("POST", TOKEN_BASE_URL, { statusCode: 400, body: { error: "invalid_grant" } }).as(
      "getToken",
    );

    fillDetails();
    cy.get('button[type="submit"]').click();
    cy.contains("University account not found").should("be.visible");
  });

  describe("check general accessibility and navigation", () => {
    it("should be accessible", () => {
      checkAccessibility({}, ["heading-order", "page-has-heading-one"]);
    });

    it("goes to the previous page when clicking 'Previous'", () => {
      cy.get("button").contains("Previous").click();
      cy.contains("Enter your University account details").should("be.visible");
    });
    it("goes to the previous page when clicking 'Re-enter your account details'", () => {
      cy.get("button").contains("Re-enter your account details").click();
      cy.contains("Enter your University account details").should("be.visible");
    });
  });
});

// Prevent TypeScript from reading file as legacy script
export {};
