import { checkAccessibility } from "../support/accessibilityTests";

describe("Home", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.get("#modal-scroll-button").click();
    cy.get("#dismiss-banner-button").click();
    cy.contains("Get your University IT account sign in details").should("be.visible");
  });

  it("should be accessible", () => {
    checkAccessibility({}, ["heading-order", "page-has-heading-one"]);
  });

  it("should navigate to the user identity option selector", () => {
    cy.get("button").contains("Get your University IT account sign in details").click();
    cy.contains("Your University CRSid").should("be.visible");
  });
});
