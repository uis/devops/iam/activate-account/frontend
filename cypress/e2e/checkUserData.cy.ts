import { checkAccessibility } from "../support/accessibilityTests";
import { fillDetails, setStandardIntercepts, TOKEN_BASE_URL } from "./utilities";

describe("UserData Component", () => {
  const ucamObserveRemoteServerLogUrl = `${Cypress.env("UCAM_OBSERVE_REMOTE_SERVER_BASE_URL")}/log`;

  beforeEach(() => {
    cy.intercept("POST", ucamObserveRemoteServerLogUrl, {
      statusCode: 200,
      body: {},
    }).as("postLog");
    setStandardIntercepts();
    cy.visit("/", {
      onBeforeLoad: (win) => {
        cy.stub(win, "alert").as("alert");
      },
    });
    cy.get("#modal-scroll-button").click();
    cy.get("#dismiss-banner-button").click();
    cy.get("button").contains("Get your University IT account sign in details").click();
  });

  describe("check general accessibility and navigation", () => {
    it("should be accessible", () => {
      checkAccessibility({}, ["heading-order", "page-has-heading-one"]);
    });

    it("goes to the previous page when clicking 'Previous'", () => {
      cy.get(`input[value='yes']`).first().check();
      cy.get('button[type="submit"]').click();
      cy.get("button").contains("Previous").click();
      cy.contains("Your University CRSid").should("be.visible");
    });
  });

  describe("With 'Do you have a CRSid?': yes", () => {
    beforeEach(() => {
      cy.get(`input[value='yes']`).first().check();
      cy.get('button[type="submit"]').click();
      cy.contains("Enter your University account details").should("be.visible");
    });

    it("shows validation error when fields are empty", () => {
      cy.get('button[type="submit"]').click();
      cy.contains("Enter your CRSid").should("be.visible");
      cy.get("@alert").should("not.have.been.called");
    });
  });

  describe("With 'Do you have a CRSid?': no", () => {
    beforeEach(() => {
      cy.get(`input[value='no']`).first().check();
      cy.get('button[type="submit"]').click();
      cy.contains("Enter your University account details").should("be.visible");
    });

    it("shows validation error when last name is 101 characters long", () => {
      cy.get('input[name="lastName"]').type("a".repeat(101));
      cy.get('button[type="submit"]').click();
      cy.contains("Last name must contain at most 100 characters").should("be.visible");
      cy.get("@alert").should("not.have.been.called");
    });
  });

  describe("Submission", () => {
    beforeEach(() => {
      cy.get(`input[value='yes']`).first().check();
      cy.get('button[type="submit"]').click();
      cy.contains("Enter your University account details").should("be.visible");
    });

    it("Successful login", () => {
      fillDetails();
      cy.get('button[type="submit"]').click();
      cy.contains("Confirm your details");
    });

    it("Invalid grant", () => {
      cy.intercept("POST", TOKEN_BASE_URL, {
        statusCode: 400,
        body: { error: "invalid_grant" },
      }).as("getToken");

      fillDetails();
      cy.get('button[type="submit"]').click();
      cy.contains("University account not found").should("be.visible");
    });

    [404, 500].forEach((statusCode) => {
      it(`Server error ${statusCode}`, () => {
        cy.intercept("POST", TOKEN_BASE_URL, { statusCode }).as("getToken");

        fillDetails();
        cy.get('button[type="submit"]').click();
        cy.contains("Sorry, there is a problem with this service").should("be.visible");
        cy.wait("@postLog").its("response.statusCode").should("eq", 200);
      });
    });
  });

  describe("Validation Errors", () => {
    beforeEach(() => {
      cy.get(`input[value='yes']`).first().check();
      cy.get('button[type="submit"]').click();
      cy.contains("Enter your University account details").should("be.visible");
    });

    it("shows validation error when using invalid date", () => {
      cy.get('input[name="date.day"]').type("31");
      cy.get('input[name="date.month"]').type("2");
      cy.get('input[name="date.year"]').type("2000");
      cy.get('button[type="submit"]').click();
      cy.contains("Invalid date").should("be.visible");
      cy.get("@alert").should("not.have.been.called");
    });

    it("shows validation error when using a future date", () => {
      const tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
      cy.get('input[name="date.day"]').type(tomorrow.getDate().toString());
      cy.get('input[name="date.month"]').type((tomorrow.getMonth() + 1).toString());
      cy.get('input[name="date.year"]').type(tomorrow.getFullYear().toString());
      cy.get('button[type="submit"]').click();
      cy.contains("Date cannot be in the future").should("be.visible");
      cy.get("@alert").should("not.have.been.called");
    });

    it("shows validation error when CRSid is 21 characters long", () => {
      cy.get('input[name="crsid"]').type("a".repeat(21));
      cy.get('button[type="submit"]').click();
      cy.contains("CRSid must contain at most 20 characters").should("be.visible");
      cy.get("@alert").should("not.have.been.called");
    });
  });
});

// Prevent TypeScript from reading file as legacy script
export {};
