import { checkAccessibility } from "../support/accessibilityTests";
import { fillDetails, setStandardIntercepts } from "./utilities";

describe("UserAccountOverview Component", () => {
  const tokenRedirectUrl = Cypress.env("TOKEN_REDIRECT_URL");

  beforeEach(() => {
    // Setup API intercepts
    setStandardIntercepts();

    // Navigate to page
    cy.visit("/", {
      onBeforeLoad: (win) => {
        cy.stub(win, "alert").as("alert");
      },
    });

    cy.window().then((win) => {
      if (win.navigator.clipboard) {
        cy.stub(win.navigator.clipboard, "writeText").as("writeTextStub");
      } else {
        win.navigator.clipboard = {
          writeText: cy.stub().as("writeTextStub"),
        };
      }
    });
    cy.get("#modal-scroll-button").click();
    cy.get("#dismiss-banner-button").click();
    cy.get("button").contains("Get your University IT account sign in details").click();
    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();
    fillDetails();
    cy.get('button[type="submit"]').click();
    cy.get(`input[value='yes']`).first().check();
    cy.get('button[type="submit"]').click();
    cy.contains("Review and agree to the Acceptable Use Policy").should("be.visible");
    cy.get('[type="checkbox"]').check({ force: true });
    cy.get('button[type="submit"]').click();
  });

  describe("Check general accessibility and navigation", () => {
    it("should be accessible", () => {
      checkAccessibility({}, ["heading-order", "page-has-heading-one"]);
      cy.contains("Account verified").should("be.visible");
    });

    it("goes to the previous page when clicking 'Previous'", () => {
      cy.get("button").contains("Previous").click();
      cy.contains("Get your University IT account").should("be.visible");
    });
  });

  describe("Check data is displayed", () => {
    it("should display the account details", () => {
      cy.contains("ab1234").should("be.visible");
    });
  });

  describe("Should redirect to password manager", () => {
    it("should redirect to password manager with the correct parameters", () => {
      cy.get("button").contains("Create a new password").click();

      cy.origin(tokenRedirectUrl, () => {
        cy.url().should((url) => {
          const regex = /reset-passwd\?crsid=ab1234&token=TEST-TEST-TEST-TEST/;
          expect(url).to.match(regex);
        });
      });
    });
  });
});
