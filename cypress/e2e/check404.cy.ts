describe("Not existing url", () => {
  beforeEach(() => {
    cy.visit("/this-page-does-not-exist", {});
  });
  it("should be redirected to the main page", () => {
    cy.contains("Get your University IT account sign in details").should("be.visible");
    cy.url().should("eq", Cypress.config().baseUrl + "/");
  });
});
