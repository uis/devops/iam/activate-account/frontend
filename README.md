This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

### Setting Up Environment Variables

Before running the development or production server, you need to set up environment variables. Copy the example secrets file and adjust the values as needed:

```bash
cp secrets.env.example secrets.env
```

Ensure that the `API_BASE_PATH` variable is set to the correct URL of the Activate Account API.

### Running the Development Server

To ensure consistency and reproducibility, it is recommended to use Docker for running the development server.
Set up Docker on your machine if you haven't already and use the following command:

```bash
npm run up
```

This will install all dependencies, and start the development server.
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

### Running the Production Server

To run the server in production mode using Docker Compose (through npm scripts):

```bash
npm run up:production
```

The production image is hosted behind traefik at `http://myaccount.docker.localhost/get-your-account/`
to simulate the GCP loadbalancer environment.

`http://traefik.docker.localhost` and `http://whoami.docker.localhost` are also available to assist
debugging custom domain and path issues.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Pre-commit Setup

To ensure code consistency and quality, this project uses pre-commit hooks. Follow these steps to set up:

1. Install the pre-commit package if you haven't already:

   ```bash
   pip install pre-commit
   ```

2. Install the pre-commit hooks:

   ```bash
   pre-commit install --hook-type pre-commit --hook-type commit-msg
   ```

3. Run the pre-commit hooks manually to check existing files (through npm scripts):
   ```bash
   npm run fix
   ```

This setup will automatically validate your commit messages against our project standards whenever you commit changes.
