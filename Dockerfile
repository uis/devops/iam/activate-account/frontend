# Base stage
FROM node:18-bookworm-slim AS base

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the entrypoint script and ensure it's executable
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Development build
FROM base AS development

# Use the entrypoint to generate config
ENTRYPOINT ["/entrypoint.sh"]

CMD ["npm", "run", "dev"]

# Cypress test stage
FROM base AS cypress

# Install Cypress dependencies
RUN apt-get update && apt-get install -y \
    libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb

# Copy Cypress files
COPY cypress cypress
COPY cypress.config.ts .

# Copy the application code
COPY . .

# Set environment variable for Cypress
ENV CYPRESS_BASE_URL=http://app:3000

# Run Cypress tests
ENTRYPOINT ["npm", "run", "cy:run"]
CMD []

# Builder stage for production build
FROM base AS builder
COPY . .

ARG HOSTED_PATH=/get-your-account

# Build your Next.js app
RUN npm run build

# Stage 2: Production environment
FROM nginx:alpine AS production

# Set the working directory
WORKDIR /usr/share/nginx/html

# Copy the built artifacts from the builder stage
COPY --from=builder /app/out /usr/share/nginx/html
COPY --from=builder /entrypoint.sh /entrypoint.sh

# Copy the nginx configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Expose the port nginx is listening on
EXPOSE 8080

# Ensure the public directory exists for the entrypoint to write to
RUN mkdir ./public

ENTRYPOINT ["/entrypoint.sh"]

# Start nginx
CMD ["nginx", "-g", "daemon off;"]
