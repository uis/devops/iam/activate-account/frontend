# Changelog

## [0.26.2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.26.1...0.26.2) (2025-03-18)

## [0.26.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.26.0...0.26.1) (2025-03-13)

### Bug Fixes

* update GA init code ([a95164f](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/a95164f69c30d62197abd6e43c014b1fcd2fd250))

## [0.26.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.25.3...0.26.0) (2025-03-03)

### Features

* use auto generated api client library ([93fad31](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/93fad31ad40dca4bfd60a64c268e57f2a287d558))

## [0.25.3](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.25.2...0.25.3) (2025-02-25)

## [0.25.2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.25.1...0.25.2) (2025-02-25)

### Bug Fixes

* add missing recaptcha token header for revoke call ([208dd76](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/208dd76ec315b4efe6da043045cb41182f5d33ac))

## [0.25.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.25.0...0.25.1) (2025-02-25)

### Bug Fixes

* update recaptcha script tag ([8f7c002](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/8f7c0022e725d237a935384f9866e0aea7b72527))

## [0.25.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.24.0...0.25.0) (2025-02-19)

### Features

* add recaptcha action token retrieval ([144d60a](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/144d60a1e650db6ae19e753413a938878c29f2be))

## [0.24.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.23.0...0.24.0) (2025-02-10)

### Features

* log requests that fail with status code 404 and >=500 ([bd077cc](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/bd077cce20b806318ef3a6d7d25b8e720a5c4901))

### Bug Fixes

* error page try again button to return to home ([8b07ff3](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/8b07ff3a2bb52e347f2ed51e095faf5718ce9ee5))
* only log uncaught error once ([a3d940f](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/a3d940ff5fb364a58a1b61e55f6e57b8cc65bcfa))

## [0.23.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.22.0...0.23.0) (2025-02-06)

### Features

* e2e tests use a production build ([34f6f98](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/34f6f98f81271911fb14fcc30d6583c46e7d2bf3))

### Bug Fixes

* missing intercept of revoke token call ([c2e2495](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/c2e24951132dea6ee51af5cd233f3897418249d8))

## [0.22.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.21.0...0.22.0) (2025-02-04)

### Features

* app expected to run under /get-your-account/ path ([f9a35bc](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/f9a35bc21e4ce25b582f3212bcba04dadf4ab8f4))
* build static export and host with nginx ([877e6e1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/877e6e13f18a4df044cca650d203749fc8224eb1))
* docker compose host production using traefik ([b0b9780](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/b0b978006c1989b439cebd14e70aba2e84f7cb72))

### Bug Fixes

* nextjs running under path ([82db19e](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/82db19ee9a98902341597fe6b58431ffca1cecf1))

## [0.21.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.20.0...0.21.0) (2025-01-24)

### Features

* call reset token endpoint ([5497b94](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/5497b94f5097764f1845dda04a53c3ba1891d27a))
* e2e tests check correct password app url and reset token failure ([f61d790](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/f61d79041dc6273072809ea0d298b3aa64b8f5c3))
* move password app redirect url to config from constant ([aba9104](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/aba91047077427420bd9353fd63f1de31fa51f3b))

## [0.20.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.19.0...0.20.0) (2025-01-20)

### Features

* **UniversityAccountRegistration:** update landing page Button ([afc9532](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/afc9532cfff166256e84c5d7cd79f63305e30652))
* **UniversityAccountRegistration:** update landing page content ([062e3d4](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/062e3d468375cdb277ecad0e300533f6ce92ecc3))

## [0.19.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.18.0...0.19.0) (2025-01-15)

### Features

* **UserAccountOverview:** replace InfoIcon with ErrorIcon ([c6b1a42](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/c6b1a424d2594079644c45ae346fe6206aee3550))

## [0.18.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.17.0...0.18.0) (2025-01-14)

### Features

* add @mui/icons-material ([65ab668](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/65ab6683f5bd034c76bc63afa8f8b99f96ac4278))
* **UserAccountOverview:** add loading spinner and simplify account details display logic ([640ee92](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/640ee928b5ac1d8c5e431ef16606ddc1c7a41e78))
* **UserAccountOverview:** update account verification UI ([c87175e](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/c87175e9d1455ce238230c524bf27ef95d529851))

## [0.17.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.16.1...0.17.0) (2025-01-08)

### Features

* **UserAccountDetails:** update UI labels and structure for clarity ([27f1d61](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/27f1d613192edd1860e5342ff040071591d95e9b))

## [0.16.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.16.0...0.16.1) (2025-01-08)

## [0.16.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.15.0...0.16.0) (2025-01-08)

### Features

* amended content around advice on codes for staff and students ([d10b780](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/d10b780f8128cc14efd64116d0afcefaf5163415))
* **UserIdentityOptionSelector:** update CRSid question wording and radio button options ([eb71f07](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/eb71f07474340665ed66369df5304e7486594688))

### Bug Fixes

* **UserIdentityOptionSelector:** reformat Link component ([e9261bc](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/e9261bc8bcc026d3599bdf55f73c6085179b417c))

## [0.15.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.14.0...0.15.0) (2025-01-03)

### Features

* **terms-of-use:** update page to improve clarity and soften tone ([f64fd65](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/f64fd657d9d5a8a4b9afffaed35a8dc3e31a3919))

## [0.14.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.13.0...0.14.0) (2024-12-10)

### Features

* add Remote and Front-end Analytics ([053bf91](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/053bf9199f63bdfb6bcfee0a7719f45d92029631))

### Bug Fixes

* add `language_version` for `eslint` & `prettier` hooks ([5e1274c](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/5e1274cd42068571d21cc52b2d001c364da79960))

## [0.13.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.12.0...0.13.0) (2024-12-06)

### Features

* add ErrorBoundary ([f2c00ec](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/f2c00ec2a2b97be7ac676498a67b2c60d4e60ac2))

## [0.12.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.11.0...0.12.0) (2024-12-02)

### Features

* update content for 5xx page ([ad77ace](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/ad77acead1f2d2e640a60a5bb8cd1b89535675a7))

## [0.11.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.10.0...0.11.0) (2024-11-21)

### Features

* enhance `SessionExpired` component with additional content ([11e838d](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/11e838d5f669fd5c1d05b34378530c248752832c))

## [0.10.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.9.1...0.10.0) (2024-11-11)

### Features

* add step with ErrorPage ([06dd553](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/06dd55314b9da57b50ed997809c493b92a4ad05d))

### Bug Fixes

* **auth:** change revoke endpoint request method from GET to POST ([156efa6](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/156efa639a6f788b330172c33dfc4ee3f0c3ff3b)), closes [heads#L81](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/heads/issues/L81)
* **auth:** handle 404 and server error responses in axios client ([5abc237](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/5abc2375a07a2fc2adc6d377c0fb7210811adc5b))

## [0.9.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.9.0...0.9.1) (2024-11-09)

## [0.9.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.8.3...0.9.0) (2024-11-08)

### Features

* add redirection logic and test for non-root paths ([3d4b384](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/3d4b384d94ebdc9131ba8ace8b67d9dd4d9290ac))

## [0.8.3](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.8.2...0.8.3) (2024-11-05)

### Bug Fixes

* add MuiList-unordered for lists (UniversityAccountNotFound.tsx) ([633f610](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/633f6104229c60df817f3448b0ed1fd56eba0b85))

## [0.8.2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.8.1...0.8.2) (2024-11-05)

## [0.8.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.8.0...0.8.1) (2024-11-05)

## [0.8.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.7.0...0.8.0) (2024-11-05)

### Features

* add UserAccountOverview.tsx page ([5d7e288](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/5d7e2888caf49ff9ad877e95e4a50055fa649cfc))

## [0.7.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.6.2...0.7.0) (2024-11-05)

### Features

* add UniversityAccountNotFound.tsx ([8280f06](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/8280f06d358a1096006e8c55257b82386f1983f0))

## [0.6.2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.6.1...0.6.2) (2024-10-31)

## [0.6.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.6.0...0.6.1) (2024-10-31)

## [0.6.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.5.2...0.6.0) (2024-10-30)

### Features

* add user account details step ([2b50308](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/2b5030845494dd941c555f2c724c98749b13709a))

### Bug Fixes

* add padding to navigation buttons ([b699a5d](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/b699a5deb5dadce91e470e723cc0c5f9feae3811))
* correct dockerfile command casing ([554a859](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/554a859a18164263e35e3f05f318b93042ef6c85))
* remove redundant consts ([771984c](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/771984c8adecdec83d1334f1d981c004d7d94c1b))

## [0.5.2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.5.1...0.5.2) (2024-10-30)

## [0.5.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.5.0...0.5.1) (2024-10-28)

### Bug Fixes

* make eslint run prettier ([086b06a](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/086b06aab082da24b1cffdc5eba917fba4325093))
* use appropriate pre-commit hook ([8693f4b](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/8693f4b0c9bb59e644920bc37dc079e7241212e0))

## [0.5.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.4.0...0.5.0) (2024-10-28)

### Features

* implement session management/authentication to access data post-verification ([0e9eb87](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/0e9eb875a46602a7ad5b28bcf4e87ecec1443cb3))

## [0.4.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.3.5...0.4.0) (2024-10-28)

### Features

* add placeholder terms of use page ([a85a61f](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/a85a61fda83f34b6d7118c35db9dc5bcd2cb1346))

## [0.3.5](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.3.4...0.3.5) (2024-10-21)

## [0.3.4](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.3.3...0.3.4) (2024-10-17)

## [0.3.3](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.3.2...0.3.3) (2024-10-17)

### Bug Fixes

* **deps:** update all non-major dependencies ([345d818](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/345d818074cbee47d41dacfe87fd22e865d607bd))

## [0.3.2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.3.1...0.3.2) (2024-10-16)

## [0.3.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.3.0...0.3.1) (2024-10-11)

## [0.3.0](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.2.4...0.3.0) (2024-10-09)


### Features

* add labels for date, month and year fields ([bfb673a](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/bfb673a57c104fab8389cf0cb486df21ca3adcc9))
* add user data verification page ([15d3a2b](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/15d3a2b745d6d62d990a62aa40c11f8a73dd5c96))
* change datepicker to day, month, year fields ([98d22e9](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/98d22e91dbd771ba08b040a221efe2cfc21910eb))
* use date-fns for checking dates ([b3f795c](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/b3f795c1c95963c338a572932ea67f5f657976ae))
* use labels for input fields ([f4af227](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/f4af2271fe7e6fca62979c571670b172e3f2a756))


### Bug Fixes

* prettier issues ([15031c3](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/15031c3520e66fb1a5d628e3643654b325e0f8a5))
* rename commitlint configuration file ([2ca6fb4](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/2ca6fb4f1e28fc8d4f7a8e68d0595f6c8e7de5fd))

## [0.2.4](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.2.3...0.2.4) (2024-10-08)

## [0.2.3](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.2.2...0.2.3) (2024-10-04)

## [0.2.2](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.2.1...0.2.2) (2024-10-01)

## [0.2.1](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/compare/0.2.0...0.2.1) (2024-09-27)


### Bug Fixes

* add release-it support ([87bc990](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/87bc99054ddd4311898005552ad5d5dfbce677a6))
* remove bumper plugin from release-it ([f1ad7f4](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/f1ad7f4129808c63eeeb047c195cb1858304dfd8))
* update version to actual and add changelog ([a0dd290](https://gitlab.developers.cam.ac.uk/uis/devops/iam/activate-account/frontend/commit/a0dd2909493ebb4334fdf4bdd605d4bbb7b35f99))

## 0.2.0 (2024-09-24)

### Added
- UCam Observe logging

## 0.1.0 (2024-09-13)

### Added
- Initialised project
