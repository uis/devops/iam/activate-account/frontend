#!/bin/sh

# Generate the config.json file from environment variables
cat <<EOF > ./public/config.json
{
  "API_BASE_PATH": "$API_BASE_PATH",
  "UCAM_OBSERVE_REMOTE_SERVER_BASE_URL": "$UCAM_OBSERVE_REMOTE_SERVER_BASE_URL",
  "UCAM_OBSERVE_REMOTE_SERVER_CLIENT_ID": "$UCAM_OBSERVE_REMOTE_SERVER_CLIENT_ID",
  "RECAPTCHA_KEY": "$RECAPTCHA_KEY",
  "GA_TRACKING_ID": "$GA_TRACKING_ID",
  "TOKEN_REDIRECT_URL": "$TOKEN_REDIRECT_URL",
  "HOSTED_PATH": "${HOSTED_PATH}"
}
EOF

echo "Generated config.json with the following content:"
cat ./public/config.json

# Then run the main process (Next.js)
exec "$@"
