import { defineConfig } from "cypress";

export default defineConfig({
  component: {
    devServer: {
      framework: "next",
      bundler: "webpack",
    },
    video: false,
  },

  e2e: {
    setupNodeEvents(on, config) {
      config.env.API_BASE_PATH = process.env.API_BASE_PATH;
      config.env.TOKEN_REDIRECT_URL = process.env.TOKEN_REDIRECT_URL;
      config.env.UCAM_OBSERVE_REMOTE_SERVER_BASE_URL =
        process.env.UCAM_OBSERVE_REMOTE_SERVER_BASE_URL;
      return config;
    },
  },
});
